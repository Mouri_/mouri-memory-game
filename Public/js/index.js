let boxCount;
let search;
const token = "563492ad6f91700001000001926a138a967a45f9b6c1b09e5a475147";
const gameBox = document.getElementById("game-box");
const game = document.getElementById("game");
const main = document.getElementById("main");
const highScoreElement = document.getElementById("high-score");
const matchEnd = document.getElementById("match-end");
let fetchedImages = [];
let availableCards = 0;
let highScore = 0;
let solvedCards = 0;
let clickedItem = [];

function selectExp(x) {
  main.style = "display: none;";
  search = document.getElementById("input").value;
  game.style = "display: flex;";
  boxCount = x;
  startGame(x, search);
}

function restart() {
  emptyElement(gameBox);
  main.style = "display: flex;";
  game.style = "display: none;";
  matchEnd.style = "display: none";
  highScore = 0;
  highScoreElement.innerHTML = `High score ${highScore}`;
  resetPoints();
  fetchedImages = [];
}

function resetGame() {
  emptyElement(gameBox);
  resetPoints();
  game.style = "display: flex";
  matchEnd.style = "display: none";
  createImages(fetchedImages.map(val => val));
}

function emptyElement(element) {
  element.innerHTML = "";
}

function startGame(numberOfCards, query) {
  const search = query || "colour";
  availableCards = numberOfCards * 2;
  getImages(search, numberOfCards + 1)
    .then(res => res.photos)
    .then(res => res.map(val => val.src.large))
    .then(res => res.slice(0, numberOfCards + 1))
    .then(res => {
      fetchedImages = res.map(val => val);
      return res;
    })
    .then(createImages)
    .catch(console.log);
}

function createImages(res) {
  const topImg = res.pop();
  const images = backgroundForTwo(res);
  let index = 0;
  images.forEach(imgUrl => {
    const element = document.createElement("div");
    element.className = "card";
    element.style = `background: url('${imgUrl}') center /cover;`;
    element.innerHTML = `<img id="card${index}" src="${topImg}">`;
    element.setAttribute("onClick", `clicked(${index}, '${imgUrl}')`);
    gameBox.appendChild(element);
    index++;
  });
  const boxSize = Math.min(800, (61 * availableCards) / 2);
  gameBox.style = `max-width: ${boxSize}px`;
}

function getImages(query, x) {
  const properties = {
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  return fetch(
    `https://api.pexels.com/v1/search?query=${query}&per_page=${x}`,
    properties
  ).then(res => res.json());
}

function backgroundForTwo(images) {
  const twoImagesArray = [];
  const usedOnce = [];
  const length = images.length * 2;
  for (let i = 0; i < length; i++) {
    const random = getRandomNumber(images.length) - 1;
    const randomImage = images[random];
    twoImagesArray.push(randomImage);
    if (usedOnce.includes(randomImage)) {
      images.splice(random, 1);
    } else {
      usedOnce.push(randomImage);
    }
  }
  return twoImagesArray;
}

function getRandomNumber(limit) {
  return Math.floor(Math.random() * limit + 1);
}

function clicked(x, imgUrl) {
  const card = getCard(x);
  card.style = "opacity: 0%;";
  clickedItem.push([x, imgUrl]);
  const length = clickedItem.length;
  if (length % 2 == 0) {
    const previousItem = clickedItem[length - 2];
    const y = previousItem[0];
    const previousCard = getCard(y);
    console.log(card);
    if (imgUrl == previousItem[1] && x != y) {
      solvedCards += 2;
      matched(card, previousCard, x, y);
    } else {
      setTimeout(() => {
        card.style = "opacity: 100%;";
        previousCard.style = "opacity: 100%;";
      }, 1000);
    }
  }
  if (availableCards == solvedCards) {
    wonTheMatch();
  }
}

function matched(card, previousCard, x, y) {
  const cards = document.getElementsByClassName("card");
  cards[x].removeAttribute("onclick");
  cards[y].removeAttribute("onclick");
  card.src = `./img/done.png`;
  previousCard.src = `./img/done.png`;
  card.style = "opacity: unset;";
  previousCard.style = "opacity: unset;";
  setTimeout(() => {
    card.style = "opacity: 0%;";
    previousCard.style = "opacity: 0%;";
  }, 1000);
}

function getCard(x) {
  return document.getElementById(`card${x}`);
}

function wonTheMatch() {
  const currentScore = Math.ceil((availableCards / clickedItem.length) * 100);
  const highScoreMsg = document.getElementById("high-score-msg");
  if (currentScore > highScore) {
    highScore = currentScore;
    highScoreMsg.innerHTML = `You got a new high score ${highScore}`;
  } else {
    highScoreMsg.innerHTML = `Your score ${currentScore}`;
  }
  matchEnd.style = "display: flex";
  game.style = "display: none";
  highScoreElement.innerHTML = `High score ${highScore}`;
}

function resetPoints() {
  solvedCards = 0;
  clickedItem = [];
}
