const router = require("express").Router();
const bcrypt = require("bcrypt");
const users = require("../users.json");
const API_KEY = require("../config.json").API_KEY;
const formData = require('form-data');
const Mailgun = require('mailgun.js');
const mailgun = new Mailgun(formData);
const mg = mailgun.client({username: 'api', key: process.env.MAILGUN_API_KEY || API_KEY});

const OTP = getRandomNumber(999999);

  mg.messages.create('mouri-memory-game.herokuapp.com', {
    from: "Excited User <mailgun@sandbox-123.mouri-memory-game.herokuapp.com>",
    to: ["sibom46226@nhmty.com"],
    subject: "Hello",
    text: "Testing some Mailgun awesomness!",
    html: "<h1>Testing some Mailgun awesomness!</h1>"
  })
  .then(msg => console.log(msg)) // logs response data
  .catch(err => console.log(err)); // logs any error

router.post("/", (req, res) => {
  const body = req.body;
  const salt = bcrypt.genSaltSync();
  const hasedPassword = bcrypt.hashSync(body.password, salt);
  body.password = hasedPassword;
  const OTP = this.getRandomNumber(999999);

  mg.messages.create('sandbox-123.mailgun.org', {
    from: "Excited User <mailgun@sandbox-123.mailgun.org>",
    to: ["sibom46226@nhmty.com"],
    subject: "Hello",
    text: "Testing some Mailgun awesomness!",
    html: "<h1>Testing some Mailgun awesomness!</h1>"
  })
  .then(msg => console.log(msg)) // logs response data
  .catch(err => console.log(err)); // logs any error

  if(users.hasOwnProperty(body.userName)) res.status(409).json({ error: "User_name already exists" });
  else {
    users[body.userName] = body;
    res.status(201).json({ result: "Success" });
  }  
});

function getRandomNumber(limit) {
  return Math.floor(Math.random() * limit + 1);
}

module.exports = router;