const router = require("express").Router();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const users = require("../users.json");

router.post("/", (req, res) => {
  const userName = req.body.userName;
  const password = req.body.password;

  if(!users.hasOwnProperty(body.userName)) res.status(401).json({ error: "User_name does not exist" });
  else{
      if (bcrypt.compareSync(password, users[userName].password)) {
        const token = jwt.sign(userName, process.env.JWT_SECRET || "secret");
        res.status(200).json({ token: token });
      } else {
        res.status(401).json({ error: "Wrong password" });
      }
  }
});

module.exports = router;