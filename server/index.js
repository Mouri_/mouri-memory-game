require("dotenv").config();

const express = require("express");

const jwt = require("jsonwebtoken");

const config = require("./config.json");

const authRoute = require("./routers/auth");

const userRoute = require("./routers/users");

const PORT = process.env.PORT || config.PORT;


const app = express();

app.use(express.json());

app.use("/login", authRoute);
app.use("/signUp", userRoute);

app.use("/", (req, res, next) => {
  if (!req.headers.hasOwnProperty("authorization")) {
    req.body.userName = null;
    return next();
  }

  const token = req.headers.authorization.replace("Bearer ", "");
  jwt.verify(token, process.env.JWT_SECRET || "secret", (err, userName) => {
    if (err) return res.status(401).json({ error: "Invalid token" });
    else{
        req.body.userName = userName;
        next();
    }
  });
});

app.use("/", (req, res) => {
  res.status(400).json({ error: "Bad request" });
});

app.listen(PORT, () => console.log("Express server running on port", PORT));